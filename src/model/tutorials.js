'use strict';
export default (sequelize, DataTypes) => {
    const Tutorial = sequelize.define("tutorials", {
      title: {
        type: DataTypes.STRING
      },
      description: {
        type: DataTypes.STRING
      },
      published: {
        type: DataTypes.BOOLEAN
      }
    });
  
    return Tutorial;
  };