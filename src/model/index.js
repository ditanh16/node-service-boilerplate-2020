import dbConfig from "#root/config/db";
import Sequelize from "sequelize";
import postModel from "./posts";
import tutorialModel from "./tutorials";

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.posts = postModel(sequelize, Sequelize);
db.tutorials = tutorialModel(sequelize, Sequelize);

export default db;