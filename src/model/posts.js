'use strict';
export default (sequelize, DataTypes) => {
  const posts = sequelize.define('posts', {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    tags: DataTypes.STRING,
    published: DataTypes.BOOLEAN
  }, {});
  posts.associate = function(models) {
    // associations can be defined here
  };
  return posts;
};